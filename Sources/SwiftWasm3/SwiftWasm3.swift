import Foundation
import Wasm3C

public enum Wasm3Error: Error {
    case functionNotFound(msg: String)
    case functionCallError(msg: String)
}

public class SW3Module {
    var module: UnsafeMutablePointer<IM3Module?>!
    var numFunctions: Int
    
    init(module: UnsafeMutablePointer<IM3Module?>!) {
        self.module = module
        self.numFunctions = 0
    }
}

public class SW3Environment {
    var cenv: IM3Environment
    
    public init() {
        self.cenv = m3_NewEnvironment ()
    }
    
    deinit {
        m3_FreeEnvironment(self.cenv)
    }
    
    public func parseModule(wasmbin: [UInt8]) -> SW3Module {
        let wasmbytecount = wasmbin.count
        let module = UnsafeMutablePointer<IM3Module?>.allocate(capacity: 1)
        let _ = m3_ParseModule(cenv, module, wasmbin, UInt32(wasmbytecount))

        return SW3Module(module: module)
    }
}

public struct SW3Config {
    public private(set) var env: SW3Environment
    var stackSize: UInt32
    var enableWASI: Bool
    
    public init(env: SW3Environment, stackSize: UInt32, enableWASI: Bool) {
        self.env = env
        self.stackSize = stackSize
        self.enableWASI = enableWASI
    }
    
    
}

public class SW3Function: Equatable {
    var function: UnsafeMutablePointer<IM3Function?>!
    
    init(function: UnsafeMutablePointer<IM3Function?>!) {
        self.function = function
    }
    
    public static func ==(lhs: SW3Function, rhs: SW3Function) -> Bool {
        return true
    }
    
    public func callWithArgs(args: [String]) throws {
        
        var result = m3Err_none
        //let arg1: [Int8] = [1,2,3]
        /*
        let arg1: String = "40"
        let arg2: String = "13"
        arg1.withCString {arg1Ptr in
            arg2.withCString {arg2Ptr in
                let argsp: [UnsafePointer<Int8>?] = [
                    arg1Ptr,
                    arg2Ptr
                ]
                result = m3_CallWithArgs(function.pointee, UInt32(argsp.count), argsp)
            }
        }
 */
        
        let cargs = args.map { UnsafePointer<Int8>(strdup($0)) }
        // Call C function:
        result = m3_CallWithArgs(function.pointee, UInt32(cargs.count), cargs)
        //TODO: Memory leak?
        //for ptr in cargs { free(ptr) }
        
        if result != m3Err_none {
            throw Wasm3Error.functionCallError(msg: String(cString: result!))
        }
    }
}

public class SW3Runtime {
    var result: M3Result! = m3Err_none
    var cruntime: IM3Runtime!
    var config: SW3Config
    
    public init(conf: SW3Config) {
        self.config = conf
        cruntime = m3_NewRuntime (self.config.env.cenv, self.config.stackSize, nil)
    }
    
    deinit {
        m3_FreeRuntime(self.cruntime)
    }
    
    public func loadModule(module: SW3Module) {
        let result = m3_LoadModule(cruntime, module.module!.pointee!)
    }
    
    public func findFunction(name: String) throws -> SW3Function {
        
        let function = UnsafeMutablePointer<IM3Function?>.allocate(capacity: 1)
        if let result = m3_FindFunction(function, cruntime, name.cString(using: String.Encoding.utf8)) {
            throw Wasm3Error.functionNotFound(msg: String(cString: result))
        }
        
        return SW3Function(function: function)
    }
    
    public func getStackValue() -> UInt32 {
        return cruntime.pointee.stack.load(as: UInt32.self)
    }
    
}


