// swift-tools-version:5.2
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "SwiftWasm3",
    products: [
        .library(
            name: "SwiftWasm3",
            targets: ["Wasm3C", "SwiftWasm3"]),
    ],
    dependencies: [
    ],
    targets: [
        .target(
            name: "Wasm3C",
            dependencies: [],
            path: "Sources/Wasm3C"
        ),
        .target(
            name: "SwiftWasm3",
            dependencies: ["Wasm3C"],
            path: "Sources/SwiftWasm3"
        ),
        .testTarget(
            name: "SwiftWasm3Tests",
            dependencies: ["SwiftWasm3"]
        ),
    ]
)
