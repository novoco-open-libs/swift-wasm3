import XCTest

import SwiftWasm3Tests

var tests = [XCTestCaseEntry]()
tests += SwiftWasm3Tests.allTests()
XCTMain(tests)
